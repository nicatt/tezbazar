FROM node:14

WORKDIR /usr/src/tezbazar

COPY package*.json ./

RUN npm install -g nodemon
RUN npm install

COPY . /usr/src/tezbazar

EXPOSE 3000
CMD [ "nodemon", "app.js"]
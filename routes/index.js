let express = require('express');
let router = express.Router();
const {v4: uuidV4} = require('uuid');

router.get('/', (req, res) => {
  res.render('home', {
    uuid: uuidV4()
  })
})

router.get('/room/:room', (req, res) => {
  res.render('room', {roomId: req.params.room})
})

module.exports = router;
